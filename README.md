**Download, extract, rename, move**

Use the menu at upper-right (the three dots) to Download this repository, which will download a zip archive of the entire project.
Extract the zip file, rename the directory to SimpleCalc, and copy the SimpleCalc directory to your AndroidStudioProjects directory.

**Open the project in Android Studio**

Open Android Studio, Select File, then Open�, then select the SimpleCalc project.
